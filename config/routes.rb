Rails.application.routes.draw do
 
  resources :pre_requisitos
  resources :materia
  resources :turmas
  resources :professors
  resources :alunos
  resources :usuarios
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
