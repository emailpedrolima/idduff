json.extract! turma, :id, :sigla, :dias_da_semana, :horario, :status, :created_at, :updated_at
json.url turma_url(turma, format: :json)
