json.extract! materium, :id, :departamento, :nome, :created_at, :updated_at
json.url materium_url(materium, format: :json)
