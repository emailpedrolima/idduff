class Aluno < ApplicationRecord


    has_many :turma # Um aluno pode se inscrever em várias turmas
    has_many :usuarios, as: :userable # usuario > aluno


end
