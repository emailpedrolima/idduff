class Materia < ApplicationRecord

    has_many :turma #Uma matéria pode estar associada a várias turmas
    has_many :professor #uma matéria pode ser apta a vários professores


    has_many :prerequisito_ones, :class_name => 'Prerequisito', :foreign_key => :materia_id
    has_many :pre_ones, through: :prerequisito_ones
    has_many :prerequisito_twos, :class_name => 'Prerequisito', :foreign_key => :id
    has_many :pre_twos, through: :prerequisito_twos

    # Autorelação matéria com pré-requisito


    def requisito
        User.joins('pre_requisito').where("(id = (?) or materia_id = (?)) and accept = true",id,id)
    end



end
