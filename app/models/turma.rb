class Turma < ApplicationRecord

    belongs_to :materia # Uma turma está associada a uma matéria
    belongs_to :professor # Uma turma é menistrada por um professor

    has_many :aluno # Uma turma pode ter vários alunos inscritos

end
