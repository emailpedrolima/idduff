class Professor < ApplicationRecord

    has_many :turma # Um professor pode ministrar em várias turmas
    has_many :materia # Um professor pode ser licenciado em várias matérias
    has_many :usuarios, as: :userable # usuario > professor

end
