class CreatePreRequisitos < ActiveRecord::Migration[5.2]
  def change
    create_table :pre_requisitos do |t|
      t.references :id, foreign_key: true
      t.integer :materia_id
      t.boolean :accept, default:false

      t.timestamps
    end
  end
end
