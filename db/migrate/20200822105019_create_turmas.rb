class CreateTurmas < ActiveRecord::Migration[5.2]
  def change
    create_table :turmas do |t|
      t.string :sigla
      t.string :dias_da_semana
      t.string :horario
      t.string :status
      t.references :professor, foreign_key: true
      t.references :materia, foreign_key: true

      t.timestamps
    end
  end
end
