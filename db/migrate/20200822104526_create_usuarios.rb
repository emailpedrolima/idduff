class CreateUsuarios < ActiveRecord::Migration[5.2]
  def change
    create_table :usuarios do |t|
      t.string :nome
      t.string :email
      t.string :senha
      t.string :kind
      t.references :userable, polymorphic: true

      t.timestamps
    end
  end
end
