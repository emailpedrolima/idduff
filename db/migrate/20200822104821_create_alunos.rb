class CreateAlunos < ActiveRecord::Migration[5.2]
  def change
    create_table :alunos do |t|
      t.float :cr , default: nill
      t.integer :matricula
      t.references :turma, foreign_key: true

      t.float :nota_final , default: nil
      t.float :p1 
      t.float :p2
      t.string :status


      t.timestamps
    end
  end
end
