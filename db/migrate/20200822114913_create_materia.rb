class CreateMateria < ActiveRecord::Migration[5.2]
  def change
    create_table :materia do |t|
      t.string :departamento
      t.string :nome
      t.integer :id

      t.timestamps
    end
  end
end
