class CreateProfessors < ActiveRecord::Migration[5.2]
  def change
    create_table :professors do |t|
      t.integer :matricula
      t.references :materia, foreign_key: true

      t.timestamps
    end
  end
end
