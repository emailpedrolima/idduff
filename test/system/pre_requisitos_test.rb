require "application_system_test_case"

class PreRequisitosTest < ApplicationSystemTestCase
  setup do
    @pre_requisito = pre_requisitos(:one)
  end

  test "visiting the index" do
    visit pre_requisitos_url
    assert_selector "h1", text: "Pre Requisitos"
  end

  test "creating a Pre requisito" do
    visit pre_requisitos_url
    click_on "New Pre Requisito"

    check "Accept" if @pre_requisito.accept
    fill_in "Id", with: @pre_requisito.id_id
    fill_in "Materia", with: @pre_requisito.materia_id
    click_on "Create Pre requisito"

    assert_text "Pre requisito was successfully created"
    click_on "Back"
  end

  test "updating a Pre requisito" do
    visit pre_requisitos_url
    click_on "Edit", match: :first

    check "Accept" if @pre_requisito.accept
    fill_in "Id", with: @pre_requisito.id_id
    fill_in "Materia", with: @pre_requisito.materia_id
    click_on "Update Pre requisito"

    assert_text "Pre requisito was successfully updated"
    click_on "Back"
  end

  test "destroying a Pre requisito" do
    visit pre_requisitos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Pre requisito was successfully destroyed"
  end
end
